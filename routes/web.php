<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth', 'auth.admin'])->group(function() {
    //USERS
    Route::resource('/users', 'App\Http\Controllers\UsersController')->except(['show', 'destroy']);

    //SECTIONS
    Route::resource('/sections', 'App\Http\Controllers\SectionsController')->except(['show', 'destroy']);
    
    //CONSOLIDATE
    Route::get('/consolidate', 'App\Http\Controllers\ConsolidateController@index')->name('consolidate');
    Route::get('/consolidate/{section}', 'App\Http\Controllers\ConsolidateController@show')->name('consolidate.show');
});

Route::middleware(['auth', 'auth.jury'])->group(function() {
    //PROJECTS
    Route::resource('/projects', 'App\Http\Controllers\ProjectsController');

    //CHECK
    Route::get('/check', 'App\Http\Controllers\CheckController@index')->name('checklist');
    Route::get('/check/{project}', 'App\Http\Controllers\CheckController@check')->name('check');
    Route::post('/check/{project}', 'App\Http\Controllers\CheckController@store')->name('check.store');
    Route::get('/check/{project}/edit', 'App\Http\Controllers\CheckController@edit')->name('check.edit');
    Route::get('/check/{project}/show', 'App\Http\Controllers\CheckController@show')->name('check.show');
    Route::put('/check/{project}/update', 'App\Http\Controllers\CheckController@update')->name('check.update');
});