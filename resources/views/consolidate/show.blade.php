@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Критерии
                    </button>
                </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                <table class="table table-striped">
                <thead>
                    <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                    <th scope="col">#</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">K1.1</th>
                        <td>Обоснование актуальности исследования: теоретическая значимость работы</td>
                        <td>c1_1_1</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.2</th>
                        <td>Обоснование актуальности исследования: практическая значимость работы</td>
                        <td>c1_1_0</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.3</th>
                        <td>Литературный обзор по исследуемой проблеме: соответствие теме исследования</td>
                        <td>c1_2_1</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.4</th>
                        <td>Литературный обзор по исследуемой проблеме: наличие описания истории изучения темы</td>
                        <td>c1_2_2</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.5</th>
                        <td>Уровень изложения программы и результатов исследования</td>
                        <td>c1_3_1</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.6</th>
                        <td>Обоснованность и качество представленных графиков, диаграмм и схем, иллюстраций</td>
                        <td>c1_3_2</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.7</th>
                        <td>Наличие анализа результатов исследования</td>
                        <td>c1_3_3</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.8</th>
                        <td>Соответствие выводов поставленным гипотезам</td>
                        <td>c1_3_4</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.9</th>
                        <td>Соответствие выводов поставленным целям</td>
                        <td>c1_3_5</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.10</th>
                        <td>Соответствие выводов поставленным задачам</td>
                        <td>c1_3_6</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.11</th>
                        <td>Обоснованность сформулированных выводов</td>
                        <td>c1_4</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.12</th>
                        <td>Правильность использования терминов в работе</td>
                        <td>c1_5</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.13</th>
                        <td>Культура оформления работы</td>
                        <td>c1_6</td>
                    </tr>
                    <tr>
                        <th scope="row">K1.14</th>
                        <td>Проверка на плагиат (%)</td>
                        <td>c1_plug</td>
                    </tr>
                    <tr>
                        <th scope="row">K1</th>
                        <td>Сумма баллов первой части</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">K2.1</th>
                        <td>Выступление позволяет понять суть исследования, оценить достоверность и научность полученных результатов:</td>
                        <td>c2_2</td>
                    </tr>
                    <tr>
                        <th scope="row">K2.2</th>
                        <td>Логика изложения материала. Наличие элементов структуры научного исследования (цель, задачи, гипотеза, этапы, выводы):</td>
                        <td>c2_3</td>
                    </tr>
                    <tr>
                        <th scope="row">K2.3</th>
                        <td>Эрудиция, научный стиль речи, умение использовать специальные термины, научные понятия</td>
                        <td>c2_4</td>
                    </tr>
                    <tr>
                        <th scope="row">K2.4</th>
                        <td>Ораторское искусство выступающего (умение говорить связанно, логически и художественно, чтобы привлечь внимание)</td>
                        <td>c2_5</td>
                    </tr>
                    <tr>
                        <th scope="row">K2.5</th>
                        <td>Соблюдение регламента</td>
                        <td>c2_6</td>
                    </tr>
                    <tr>
                        <th scope="row">K2.6</th>
                        <td>Качество иллюстративного материала</td>
                        <td>c2_7</td>
                    </tr>
                    <tr>
                        <th scope="row">K2.7</th>
                        <td>Сумма баллов второй части</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">K2</th>
                        <td>Общая сумма баллов</td>
                        <td></td>
                    </tr>

                </tbody>
            </table>
                </div>
                </div>
            </div>
        </div>
        <hr>
            <h2>{{ $section->name }}</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Работа</th>
                        <th scope="col">Жюри</th>
                        <th class="text-center text-vertical" scope="col">К1.1</th>
                        <th class="text-center text-vertical" scope="col">К1.2</th>
                        <th class="text-center" scope="col">К1.3</th>
                        <th class="text-center" scope="col">К1.4</th>
                        <th class="text-center" scope="col">К1.5</th>
                        <th class="text-center" scope="col">К1.6</th>
                        <th class="text-center" scope="col">К1.7</th>
                        <th class="text-center" scope="col">К1.8</th>
                        <th class="text-center" scope="col">К1.9</th>
                        <th class="text-center" scope="col">К1.10</th>
                        <th class="text-center" scope="col">К1.11</th>
                        <th class="text-center" scope="col">К1.12</th>
                        <th class="text-center" scope="col">К1.13</th>
                        <th class="text-center" scope="col">К1.14</th>
                        <th class="text-center" scope="col">K1</th>

                        <th class="text-center" scope="col">К2.1</th>
                        <th class="text-center" scope="col">К2.2</th>
                        <th class="text-center" scope="col">К2.3</th>
                        <th class="text-center" scope="col">К2.4</th>
                        <th class="text-center" scope="col">K2.5</th>
                        <th class="text-center" scope="col">K2.6</th>
                        <th class="text-center" scope="col">K2</th>
                        <th class="text-center" scope="col">Итого</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($section->projects as $project)
                        @foreach($section->users()->juriesOnly()->get() as $jury)
                        <tr>
                            @if($loop->iteration == 1)
                            <td scope="col" rowspan="{{ $section->users()->juriesOnly()->count() + 1 }}">
                                <strong>ФИО:</strong> {{ $project->fullname }}<br>
                                <strong>ОУ:</strong> {{ $project->organization }}<br>
                                <strong>Класс:</strong> {{ $project->stage }}
                            </td>
                            @endif
                            <th scope="col">Жюри {{ $loop->iteration }}</th>
                            @if(! $project->hasResultFromUser($jury->id)) 
                                <th colspan="23" class="text-center bg-warning">Не проверил</th>
                                @continue
                            @endif
                            <?php $result = $project->getResultFromUser($jury->id) ?>
                            <th class="text-center" scope="col">{{ $result->c1_1_1 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_1_2 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_2_1 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_2_2 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_3_1 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_3_2 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_3_3 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_3_4 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_3_5 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_3_6 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_4 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_5 }}</th>
                            <th class="text-center" scope="col">{{ $result->c1_6 }}</th>
                            <th class="text-center table-info" scope="col">{{ $result->c1_plug }}</th>
                            <th class="text-center table-info" scope="col">{{ $result->sumOfFirstPart() }}</th>
                            
                            <th class="text-center" scope="col">{{ $result->c2_2 }}</th>
                            <th class="text-center" scope="col">{{ $result->c2_3 }}</th>
                            <th class="text-center" scope="col">{{ $result->c2_4 }}</th>
                            <th class="text-center" scope="col">{{ $result->c2_5 }}</th>
                            <th class="text-center" scope="col">{{ $result->c2_6 }}</th>
                            <th class="text-center" scope="col">{{ $result->c2_7 }}</th>
                            <th class="text-center table-info" scope="col">{{ $result->sumOfSecondPart() }}</th>
                            <th class="text-center table-success" scope="col">{{ $result->sumOfAll() }}</th>
                        </tr>
                        @endforeach
                        <?php $results = $project->getAllResults() ?>
                        <tr class="table-success">
                            <th scope="col" class="table-success">Средний балл:</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_1_1'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_1_2'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_2_1'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_2_2'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_3_1'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_3_2'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_3_3'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_3_4'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_3_5'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_3_6'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_4'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_5'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c1_6'), 2) }}</th>
                            <th class="text-center" scope="col"></th>
                            <th class="text-center" scope="col">{{ round($project->getFirstPartScore(), 2) }}</th>

                            <th class="text-center" scope="col">{{ round($results->avg('c2_2'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c2_3'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c2_4'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c2_5'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c2_6'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($results->avg('c2_7'), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($project->getSecondPartScore(), 2) }}</th>
                            <th class="text-center" scope="col">{{ round($project->getTotalScore(), 2) }}</th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
