@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Выбор секции</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название секции</th>
                        <th class="text-center" scope="col">Количество работ</th>
                        <th class="text-center" scope="col">Прогресс проверки</th>
                        <th class="text-center" scope="col">Члены жюри</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sections as $section)
                    <tr>
                        <th scope="row">{{ $section->id }}</th>
                        <td><a href="{{ route('consolidate.show', $section->id) }}">{{ $section->name }}</a></td>
                        <td class="text-center">{{ $section->projects->count() }}</td>
                        <td class="text-center {{ $section->getCheckProgress() == 100 ? 'bg-success' : '' }}">{{ $section->getCheckProgress() }}%</td>
                        <td class="text-center">
                        @foreach($section->users()->juriesOnly()->get() as $jury)
                            {{ $jury->name }} <br>
                        @endforeach
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
