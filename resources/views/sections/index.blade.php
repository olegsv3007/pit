@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Секции</h2>
        <a href="{{ route('sections.create') }}" class="btn btn-primary mt-5 mb-5 float-right">Добавть Секцию</a>
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Название секции</th>
                <th scope="col">Активность</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @forelse($sections as $section)
                <tr>
                    <th scope="row">{{ $section->id }}</th>
                    <td>{{ $section->name }}</td>
                    <td class="{{ $section->active ? 'text-success' : 'text-danger' }}">{{ $section->active ? 'Активна' : 'Неактивна' }}</td>
                    <td><a href="{{ route('sections.edit', $section) }}">Редактировать</a></td>
                </tr>
                @empty
                    <td colspan=5>Пусто</td>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection