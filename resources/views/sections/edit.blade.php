@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Редактирование секции</h2>
            <form action="{{ route('sections.update', $section) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Название секции</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $section->name }}">
                </div>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="checkbox" value="1" name="active" id="defaultCheck1" {{ $section->active ? 'checked' : '' }}>
                    <label class="form-check-label" for="defaultCheck1">
                        Активность
                    </label>
                </div>
                <br>
                <br>
                <input type="submit" value="Сохранить изменения" class="btn btn-primary m-auto mt-5">
            </form>
        </div>
    </div>
</div>
@endsection