@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Создание Секции</h2>
            <form action="{{ route('sections.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Название секции</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <input type="submit" value="Создать Секцию" class="btn btn-primary m-auto mt-5">
            </form>
        </div>
    </div>
</div>
@endsection