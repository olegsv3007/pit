@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Изменение проекта</h2>
            <form action="{{ route('projects.update', $project->id) }}" method="post">
                @csrf
                @method('PUT')
                <input type="hidden" name="id" value="{{ $project->id }}">
                <div class="form-group">
                    <label for="section">Выберите секцию</label>
                    @error('section_id')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                    @enderror
                    <select class="custom-select" name="section_id">
                        @foreach(auth()->user()->sections as $section)
                        <option value="{{ $section->id }}" {{ $section->id == $project->section->id ? 'selected' : '' }}>{{ $section->name }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="fullname">ФИО</label>
                    @error('fullname')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="text" class="form-control" id="fullname" name="fullname" value="{{ $project->fullname }}">
                </div>
                <div class="form-group">
                    <label for="organization">Наименование образовательного учреждения </label>
                    @error('organization')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="text" class="form-control" id="organization" name="organization" value="{{ $project->organization }}">
                </div>
                <div class="form-group">
                    <label for="stage">Класс</label>
                    @error('stage')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="number" min="1" max="11" class="form-control" id="stage" name="stage" value="{{ $project->stage }}">
                </div>
                <div class="form-group">
                    <label for="city">Город (По умолчанию: Калининград)</label>
                    <input type="text" class="form-control" id="city" name="city" value="{{ $project->city }}">
                </div>
                <div class="form-group">
                    <label for="link">Ссылка на проект</label>
                    @error('link')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="text" class="form-control" id="link" name="link" value="{{ $project->link }}">
                </div>
                <input type="submit" value="Сохранить проект" class="btn btn-primary m-auto mt-5">
            </form>
        </div>
    </div>
</div>
@endsection