@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Работы</h2>
            <a href="{{ route('projects.create') }}" class="btn btn-primary mt-5 mb-5">Добавть работу</a><br>
            @foreach(auth()->user()->sections as $section)
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#section-{{ $section->id }}" aria-expanded="false" aria-controls="collapseOne">
                        {{ $section->name }}
                        <span class="badge badge-info ml-3">{{ $section->projects->count() }}</span>
                        </button>
                    </h2>
                    </div>

                    <div id="section-{{ $section->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">ФИО</th>
                                        <th scope="col">ОУ</th>
                                        <th scope="col">Класс</th>
                                        <th scope="col">Ссылка</th>
                                        <th scope="col">Редактировать</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($section->projects as $project) 
                                    <tr>
                                        <th scope="col">{{ $project->fullname }}</th>
                                        <th scope="col">{{ $project->organization }}</th>
                                        <th scope="col">{{ $project->stage }}</th>
                                        <th scope="col"><a href="{{ $project->link }}">Открыть работу</a></th>
                                        <th scope="col"><a href="{{ route('projects.edit', $project->id) }}">Редактировать</a></th>
                                    </tr>
                                @empty

                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
