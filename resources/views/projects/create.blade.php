@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Добавление работы</h2>
            <form action="{{ route('projects.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="section">Выберите секцию</label>
                    @error('section_id')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                    @enderror
                    <select class="custom-select" name="section_id">
                        @foreach(auth()->user()->sections as $section)
                        <option value="{{ $section->id }}">{{ $section->name }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="fullname">ФИО</label>
                    @error('fullname')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="text" class="form-control" id="fullname" name="fullname">
                </div>
                <div class="form-group">
                    <label for="organization">Наименование образовательного учреждения </label>
                    @error('organization')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="text" class="form-control" id="organization" name="organization">
                </div>
                <div class="form-group">
                    <label for="stage">Класс</label>
                    @error('stage')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="number" min="1" max="11" class="form-control" id="stage" name="stage">
                </div>
                <div class="form-group">
                    <label for="city">Город (По умолчанию: Калининград)</label>
                    <input type="text" class="form-control" id="city" name="city" value="Калининград">
                </div>
                <div class="form-group">
                    <label for="link">Ссылка на проект</label>
                    @error('link')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="text" class="form-control" id="link" name="link">
                </div>
                <input type="submit" value="Сохранить работу" class="btn btn-primary m-auto mt-5">
            </form>
        </div>
    </div>
</div>
@endsection