@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Оценка исследовательских работ</h2>
            @foreach(auth()->user()->sections as $section)
            <h3 class="mt-5">{{ $section->name }}</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">ФИО</th>
                        <th scope="col">ОУ</th>
                        <th scope="col">Статус</th>
                        <th scope="col">Оценки</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                @forelse($section->projects as $project)
                    <tr>
                        <th scope="row">{{ $project->id }}</th>
                        <td>{{ $project->fullname }}</td>
                        <td>{{ $project->organization }}</td>
                        
                        @if($project->hasResultFromCurrentUser())
                        <td><span class="text-success">Проверено</span></td>
                        <td><a href="{{ route('check.show', $project->id) }}">Просмотреть баллы</a></td>
                        <td><a href="{{ route('check.edit', $project->id) }}">Изменить баллы</a></td>
                        @else
                        <td><span class="text-danger">Не проверено</span></td>
                        <td></td>
                        <td><a href="{{ route('check', $project->id) }}">Выставить баллы</a></td>
                        @endif
                    </tr>
                    @empty
                    <tr>
                        <td colspan="6" class="text-center">В данной секции пока нет работ</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            @endforeach
        </div>
    </div>
</div>
@endsection
