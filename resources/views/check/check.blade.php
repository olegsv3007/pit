@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">  
        <div class="col-md-8">
            <div class="card mt-5 mb-5">
                <h5 class="card-header">Информация о работе</h5>
                <div class="card-body">
                    <h5 class="card-title">{{ $project->section->name }}</h5>
                    <p class="card-text"><strong>Имя:</strong> {{ $project->fullname }}</p>
                    <p class="card-text"><strong>Образовательное учреждение:</strong> {{ $project->organization }}</p>
                    <p class="card-text"><strong>Класс:</strong> {{ $project->stage }}</p>
                    <a target="_blank" class="btn btn-success" href="{{ $project->link }}">Ознакомиться с работой</a>
                </div>
            </div>
            <form action="{{ route('check.store', $project->id) }}" method="post">
                @csrf
                <input type="hidden" name="project_id" value="{{ $project->id }}">
                <h2 class="text-center font-weight-bold">Оценка исследовательской работы</h2>
                {{-- С1_1_1 --}}
                <div class="form-group mt-4">
                    <label>Обоснование актуальности исследования: теоретическая значимость работы</label>
                    <select name="c1_1_1" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                    </select>
                </div>

                {{-- С1_1_2 --}}
                <div class="form-group mt-4">
                    <label>Обоснование актуальности исследования: практическая значимость работы</label>
                    <select name="c1_1_2" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                    </select>
                </div>

                {{-- С1_2_1 --}}
                <div class="form-group mt-4">
                    <label>Литературный обзор по исследуемой проблеме: соответствие теме исследования</label>
                    <select name="c1_2_1" class="form-control">
                        <option selected>0</option>
                        <option>2</option>
                    </select>
                </div>

                {{-- С1_2_2 --}}
                <div class="form-group mt-4">
                    <label>Литературный обзор по исследуемой проблеме: наличие описания истории изучения  темы</label>
                    <select name="c1_2_2" class="form-control">
                        <option selected>0</option>
                        <option>2</option>
                    </select>
                </div>

                {{-- С1_3_1 --}}
                <div class="form-group mt-4">
                    <label>Уровень изложения программы и результатов исследования</label>
                    <select name="c1_3_1" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                    </select>
                </div>

                {{-- С1_3_2 --}}
                <div class="form-group mt-4">
                    <label>Обоснованность и качество представленных графиков, диаграмм и схем, иллюстраций</label>
                    <select name="c1_3_2" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>

                {{-- С1_3_3 --}}
                <div class="form-group mt-4">
                    <label>Наличие анализа результатов исследования</label>
                    <select name="c1_3_3" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>

                {{-- С1_3_4 --}}
                <div class="form-group mt-4">
                    <label>Соответствие выводов поставленным гипотезам</label>
                    <select name="c1_3_4" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                    </select>
                </div>

                {{-- С1_3_5 --}}
                <div class="form-group mt-4">
                    <label>Соответствие выводов поставленным целям</label>
                    <select name="c1_3_5" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                    </select>
                </div>

                {{-- С1_3_6 --}}
                <div class="form-group mt-4">
                    <label>Соответствие выводов поставленным задачам</label>
                    <select name="c1_3_6" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                    </select>
                </div>

                {{-- С1_4 --}}
                <div class="form-group mt-4">
                    <label>Обоснованность сформулированных выводов</label>
                    <select name="c1_4" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>

                {{-- С1_5 --}}
                <div class="form-group mt-4">
                    <label>Правильность использования терминов в работе </label>
                    <select name="c1_5" class="form-control">
                        <option selected value="0">более 2-х ошибок - 0 баллов</option>
                        <option value="1">1-2 ошибки - 1 балл</option>
                        <option value="3">0 ошибок - 3 балла </option>
                    </select>
                </div>

                {{-- С1_6 --}}
                <div class="form-group mt-4">
                    <label>Культура оформления работы</label>
                    <select name="c1_6" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>

                {{-- С1_plug --}}
                <div class="form-group">
                    <label>Проверка на плагиат (%)</label>
                    <input name="c1_plug" type="number" min="0" max="100" class="form-control" placeholder="0" value="0">
                </div>

                <h2 class="text-center font-weight-bold mt-5">Оценка выступления участника </h2>

                {{-- С2_2 --}}
                <div class="form-group mt-4">
                    <label>Выступление позволяет понять суть исследования, оценить достоверность и научность полученных результатов:</label>
                    <select name="c2_2" class="form-control">
                        <option selected>0</option>
                        <option>3</option>
                        <option>5</option>
                    </select>
                </div>
                {{-- С2_3 --}}
                <div class="form-group mt-4">
                    <label>Логика изложения материала. Наличие элементов структуры научного исследования (цель, задачи, гипотеза, этапы, выводы):</label>
                    <select name="c2_3" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                {{-- С2_4 --}}
                <div class="form-group mt-4">
                    <label>Эрудиция, научный стиль речи, умение использовать специальные термины, научные понятия</label>
                    <select name="c2_4" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                {{-- С2_5 --}}
                <div class="form-group mt-4">
                    <label>Ораторское искусство выступающего (умение говорить связанно, логически и художественно, чтобы привлечь внимание)</label>
                    <select name="c2_5" class="form-control">
                        <option selected>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                {{-- С2_6 --}}
                <div class="form-group mt-4">
                    <label>Соблюдение регламента</label>
                    <select name="c2_6" class="form-control">
                        <option selected>0</option>
                        <option>5</option>
                    </select>
                </div>
                {{-- С2_7 --}}
                <div class="form-group mt-4">
                    <label>Качество иллюстративного материала</label>
                    <select name="c2_7" class="form-control">
                        <option selected>0</option>
                        <option>5</option>
                    </select>
                </div>
                <input type="submit" value="Сохранить изменения" class="btn btn-primary m-auto mt-5">
            </form>
        </div>
    </div>
</div>
@endsection