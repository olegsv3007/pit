@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-end">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Критерий</th>
                        <th scope="col">Количество баллов</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-primary"><th scope="row" colspan="2" class="text-center">Оценка исследовательской работы</th></tr>
                    <tr>
                        <th scope="row">Обоснование актуальности исследования: теоретическая значимость работы</th>
                        <td>{{ $result->c1_1_1 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Обоснование актуальности исследования: практическая значимость работы</th>
                        <td>{{ $result->c1_1_2 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Литературный обзор по исследуемой проблеме: соответствие теме исследования</th>
                        <td>{{ $result->c1_2_1 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Литературный обзор по исследуемой проблеме: наличие описания истории изучения темы</th>
                        <td>{{ $result->c1_2_2 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Уровень изложения программы и результатов исследования</th>
                        <td>{{ $result->c1_3_1 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Обоснованность и качество представленных графиков, диаграмм и схем, иллюстраций</th>
                        <td>{{ $result->c1_3_2 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Наличие анализа результатов исследования</th>
                        <td>{{ $result->c1_3_3 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Соответствие выводов поставленным гипотезам</th>
                        <td>{{ $result->c1_3_4 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Соответствие выводов поставленным целям</th>
                        <td>{{ $result->c1_3_5 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Соответствие выводов поставленным задачам</th>
                        <td>{{ $result->c1_3_6 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Обоснованность сформулированных выводов</th>
                        <td>{{ $result->c1_4 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Правильность использования терминов в работе</th>
                        <td>{{ $result->c1_5 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Культура оформления работы</th>
                        <td>{{ $result->c1_6 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Проверка на плагиат (%)</th>
                        <td>{{ $result->c1_plug }}</td>
                    </tr>
                    <tr class="table-success">
                        <th scope="row">Сумма баллов за первую часть</th>
                        <th>{{ $result->sumOfFirstPart() }}</th>
                    </tr>
                    <tr class="table-primary"><th scope="row" colspan="2" class="text-center">Оценка выступления участника</th></tr>
                    <tr>
                        <th scope="row">Выступление позволяет понять суть исследования, оценить достоверность и научность полученных результатов:</th>
                        <td>{{ $result->c2_2 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Логика изложения материала. Наличие элементов структуры научного исследования (цель, задачи, гипотеза, этапы, выводы):</th>
                        <td>{{ $result->c2_3 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Эрудиция, научный стиль речи, умение использовать специальные термины, научные понятия</th>
                        <td>{{ $result->c2_4 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Ораторское искусство выступающего (умение говорить связанно, логически и художественно, чтобы привлечь внимание)</th>
                        <td>{{ $result->c2_5 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Соблюдение регламента</th>
                        <td>{{ $result->c2_6 }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Качество иллюстративного материала</th>
                        <td>{{ $result->c2_7 }}</td>
                    </tr>
                    <tr class="table-success">
                        <th scope="row">Итоговая сумма баллов</th>
                        <th>{{ $result->sumOfAll() }}</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection