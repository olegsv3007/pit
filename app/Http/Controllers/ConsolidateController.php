<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Section;
use App\Models\Project;
use App\Models\User;

class ConsolidateController extends Controller
{
    public function index()
    {
        $sections = Section::active()->get();
        return view('consolidate.sections', compact('sections'));
    }

    public function show(Section $section)
    {
        return view('consolidate.show', compact('section'));
    }
}
