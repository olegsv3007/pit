<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Result;

class CheckController extends Controller
{
    public function index()
    {
        return view('check.index');
    }

    public function check(Project $project)
    {
        if (! \Gate::allows('work-with-project', $project->section->id)) {
            return abort(403);
        }
        return view('check.check', compact('project'));
    }

    public function store(Request $request)
    {   
        $project = Project::findOrFail($request->input('project_id'));

        if (! \Gate::allows('work-with-project', $project->section->id)) {
            return abort(403);
        }

        $result = Result::create([
            'jury_id' => auth()->user()->id,
            'project_id' => $request->input('project_id'),
            'c1_1_1' => $request->input('c1_1_1'),
            'c1_1_2' => $request->input('c1_1_2'),
            'c1_2_1' => $request->input('c1_2_1'),
            'c1_2_2' => $request->input('c1_2_2'),
            'c1_3_1' => $request->input('c1_3_1'),
            'c1_3_2' => $request->input('c1_3_2'),
            'c1_3_3' => $request->input('c1_3_3'),
            'c1_3_4' => $request->input('c1_3_4'),
            'c1_3_5' => $request->input('c1_3_5'),
            'c1_3_6' => $request->input('c1_3_6'),
            'c1_4' => $request->input('c1_4'),
            'c1_5' => $request->input('c1_5'),
            'c1_6' => $request->input('c1_6'),
            'c1_plug' => $request->input('c1_plug'),
            'c2_2' => $request->input('c2_2'),
            'c2_3' => $request->input('c2_3'),
            'c2_4' => $request->input('c2_4'),
            'c2_5' => $request->input('c2_5'),
            'c2_6' => $request->input('c2_6'),
            'c2_7' => $request->input('c2_7'),

        ]);

        return redirect(route('checklist'));
    }

    public function show(Project $project)
    {
        if (! \Gate::allows('work-with-project', $project->section->id)) {
            return abort(403);
        }

        $result = $project->getResultFromCurrentUser();
        return view('check.show', compact(['project', 'result']));
    }

    public function edit(Project $project)
    {
        if (! \Gate::allows('work-with-project', $project->section->id)) {
            return abort(403);
        }

        $result = $project->getResultFromCurrentUser();
        return view('check.edit', compact(['project', 'result']));
    }

    public function update(Project $project, Request $request)
    {
        if (! \Gate::allows('work-with-project', $project->section->id)) {
            return abort(403);
        }
        
        \DB::table('results')->updateOrInsert([
            'project_id' => $project->id,
            'jury_id' => auth()->user()->id,
        ], [
            'c1_1_1' => $request->input('c1_1_1'),
            'c1_1_2' => $request->input('c1_1_2'),
            'c1_2_1' => $request->input('c1_2_1'),
            'c1_2_2' => $request->input('c1_2_2'),
            'c1_3_1' => $request->input('c1_3_1'),
            'c1_3_2' => $request->input('c1_3_2'),
            'c1_3_3' => $request->input('c1_3_3'),
            'c1_3_4' => $request->input('c1_3_4'),
            'c1_3_5' => $request->input('c1_3_5'),
            'c1_3_6' => $request->input('c1_3_6'),
            'c1_4' => $request->input('c1_4'),
            'c1_5' => $request->input('c1_5'),
            'c1_6' => $request->input('c1_6'),
            'c1_plug' => $request->input('c1_plug'),
            'c2_2' => $request->input('c2_2'),
            'c2_3' => $request->input('c2_3'),
            'c2_4' => $request->input('c2_4'),
            'c2_5' => $request->input('c2_5'),
            'c2_6' => $request->input('c2_6'),
            'c2_7' => $request->input('c2_7'),
        ]);

        return redirect(route('checklist'));
    }
}
