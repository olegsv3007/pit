<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;

class ProjectsController extends Controller
{
    public function index()
    {
        return view('projects.index');
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'section_id' => 'required',
            'fullname' => 'required',
            'organization' => 'required',
            'stage' => 'required|integer|min:1|max:11',
            'city' => '',
            'link' => 'required',
        ],[
            'section_id.required' => 'Секция не выбрана',
            'fullname.required' => 'Укажите Фамилию, Имя и Отчество автора',
            'organization.required' => 'Укажите наименование образовательного учреждения',
            'stage.required' => 'Укажите класс',
            'stage.integer' => 'Укажите только цифру',
            'link.required' => 'Укажите ссылку на проект',
        ]);

        if (! \Gate::allows('work-with-project', $request->input('section_id'))) {
            return abort(403);
        }

        $project = Project::create($validatedData);

        return redirect(route('projects.index'));
    }

    public function edit(Project $project)
    {
        if (! \Gate::allows('work-with-project', $project->section->id)) {
            return abort(403);
        }

        return view('projects.edit', compact(['project']));
    }

    public function update(Request $request, Project $project)
    {
        if (! \Gate::allows('work-with-project', $request->input('section_id'))) {
            return abort(403);
        }

        $validatedData = $request->validate([
            'section_id' => 'required',
            'fullname' => 'required',
            'organization' => 'required',
            'stage' => 'required|integer',
            'city' => '',
            'link' => 'required',
        ],[
            'section_id.required' => 'Секция не выбрана',
            'fullname.required' => 'Укажите Фамилию, Имя и Отчество автора',
            'organization.required' => 'Укажите наименование образовательного учреждения',
            'stage.required' => 'Укажите класс',
            'stage.integer' => 'Укажите только цифру',
            'link.required' => 'Укажите ссылку на проект',
        ]);

        $project->update($validatedData);

        return redirect(route('projects.index'));
    }
}
