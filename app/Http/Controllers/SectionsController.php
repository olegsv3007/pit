<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Section;

class SectionsController extends Controller
{
    public function index() 
    {
        $sections = Section::all();
        return view('sections.index', compact(['sections']));
    }

    public function create() 
    {
        return view('sections.create');
    }

    public function store(Request $request) 
    {
        $sections = Section::create([
            'name' => $request->input('name'),
        ]);

        return redirect(route('sections.index'));
    }

    public function edit(Section $section) 
    {
        return view('sections.edit', compact(['section']));
    }

    public function update(Request $request, Section $section) 
    {
        $section->name = $request->input('name');
        $section->active = $request->input('active') ?? 0;
        $section->save();

        return redirect(route('sections.index'));
    }
}

