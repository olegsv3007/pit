<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \App\Models\User;
use \App\Models\Role;
use \App\Models\Section;

class UsersController extends Controller
{
    public function index() 
    {
        $users = User::all();
        return view('users.index', compact(['users']));
    }

    public function create() 
    {
        $roles = Role::all();
        $sections = Section::all();
        return view('users.create', compact(['roles', 'sections']));
    }

    public function store(Request $request) 
    {
        $user = User::create([
            'name' => $request->input('login'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        $user->roles()->attach($request->input('roles'));
        $user->sections()->attach($request->input('sections'));

        return redirect(route('users.index'));
    }

    public function edit(User $user) 
    {
        $roles = Role::all();
        $sections = Section::all();
        return view('users.edit', compact(['user', 'roles', 'sections']));
    }

    public function update(Request $request, User $user) 
    {
        $user->name = $request->input('login');
        $user->email = $request->input('email');

        if ($request->has('password') && $request->input('password') != '') {
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();
        $user->roles()->sync($request->input('roles'));
        $user->sections()->sync($request->input('sections'));

        return redirect(route('users.index'));
    }
}
