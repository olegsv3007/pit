<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthAsJury
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!(auth()->user()->hasRole('jury') || auth()->user()->hasRole('admin')) ?? true) {
            abort(403);
        }
        return $next($request);
    }
}
