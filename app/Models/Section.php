<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Result;

class Section extends Model
{
    use HasFactory;

    public $fillable = ['name', 'active'];

    public function scopeActive($query) 
    {
        return $query->where('active', 1);
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_section');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }

    public function getCheckProgress()
    {
        $projectIds = $this->projects->pluck('id');
        $resultsQuantity = Result::whereIn('project_id', $projectIds)->count();
        if ($this->projects->count() == 0 || $this->users->count() == 0) {
            return 100;
        }
        return round($resultsQuantity / ($this->users->count() * $this->projects->count()), 2) * 100;
    }
}
