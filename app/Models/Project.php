<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Result;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['fullname', 'organization', 'stage', 'section_id', 'link', 'city'];

    public function section()
    {
        return $this->belongsTo('App\Models\Section');
    }

    public function getResultFromUser($userId)
    {
        return Result::where('project_id', $this->id)->where('jury_id', $userId)->firstOrFail();
    }

    public function hasResultFromUser($userId)
    {
        if (Result::where('project_id', $this->id)->where('jury_id', $userId)->get()->isEmpty()) {
            return false;
        }
        return true;
    }

    public function hasResultFromCurrentUser()
    {
        return $this->hasResultFromUser(auth()->user()->id);
    }

    public function getResultFromCurrentUser()
    {
        return $this->getResultFromUser(auth()->user()->id);
    }

    public function getAllResults()
    {
        return Result::where('project_id', $this->id)->get();
    }

    public function getFirstPartScore()
    {
        $results = $this->getAllResults();
        $score = $results->avg(function($result) {
            return $result->sumOfFirstPart();
        });
        return $score;
    }

    public function getSecondPartScore()
    {
        $results = $this->getAllResults();
        $score = $results->avg(function($result) {
            return $result->sumOfSecondPart();
        });
        return $score;
    }

    public function getTotalScore()
    {
        $results = $this->getAllResults();
        $score = $results->avg(function($result) {
            return $result->sumOfAll();
        });
        return $score;
    }
}
