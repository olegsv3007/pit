<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function jury()
    {
        return $this->belongsTo('App\Models\User', 'jury_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }

    public function sumOfFirstPart()
    {
        return $this->c1_1_1 + 
            $this->c1_1_2 + 
            $this->c1_2_1 + 
            $this->c1_2_2 + 
            $this->c1_3_1 + 
            $this->c1_3_2 + 
            $this->c1_3_3 + 
            $this->c1_3_4 + 
            $this->c1_3_5 + 
            $this->c1_3_6 + 
            $this->c1_4 + 
            $this->c1_5 + 
            $this->c1_6;
    }

    public function sumOfSecondPart()
    {
        return $this->c2_2 + 
        $this->c2_3 + 
        $this->c2_4 + 
        $this->c2_5 + 
        $this->c2_6 + 
        $this->c2_7; 
    }

    public function sumOfAll()
    {
        return $this->sumOfFirstPart() + $this->sumOfSecondPart();
    }
}
