<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       \DB::table('roles')->insert([
            'name' => 'admin',
        ]);

        \DB::table('roles')->insert([
            'name' => 'jury',
        ]);

        \DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'metod_it@edu.klgd.ru',
            'password' => \Hash::make('fybubkzwbz'),
        ]);

        \DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1,
        ]);
    }
}
