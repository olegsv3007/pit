<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('jury_id');
            $table->tinyInteger('c1_1_1');
            $table->tinyInteger('c1_1_2');
            $table->tinyInteger('c1_2_1');
            $table->tinyInteger('c1_2_2');
            $table->tinyInteger('c1_3_1');
            $table->tinyInteger('c1_3_2');
            $table->tinyInteger('c1_3_3');
            $table->tinyInteger('c1_3_4');
            $table->tinyInteger('c1_3_5');
            $table->tinyInteger('c1_3_6');
            $table->tinyInteger('c1_4');
            $table->tinyInteger('c1_5');
            $table->tinyInteger('c1_6');
            $table->tinyInteger('c1_plug');
            $table->tinyInteger('c2_2');
            $table->tinyInteger('c2_3');
            $table->tinyInteger('c2_4');
            $table->tinyInteger('c2_5');
            $table->tinyInteger('c2_6');
            $table->tinyInteger('c2_7');

            $table->index(['project_id', 'jury_id']);
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('jury_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
